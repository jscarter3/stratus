package cfutil

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"text/tabwriter"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/fatih/color"
)

type EventTailer struct {
	session     *CFSession
	stackName   string
	eventIds    map[string]bool
	running     bool
	ticker      *time.Ticker
	stackStatus string
	wg          sync.WaitGroup
}

func (session *CFSession) NewEventTail(stackName string) *EventTailer {
	return &EventTailer{
		session:   session,
		stackName: stackName,
		running:   false,
	}
}

func (et *EventTailer) StackStatus() string {
	return et.stackStatus
}

func (et *EventTailer) Start() {
	if et.running {
		return
	}
	et.running = true
	et.eventIds = make(map[string]bool)

	events, err := getCFEvents(et.session.CF, et.stackName)
	if err == nil {
		for _, e := range events {
			et.eventIds[*e.EventId] = true
		}
	}

	et.ticker = time.NewTicker(time.Second * 1)
	go func() {
		et.wg.Add(1)
		defer et.wg.Done()
		w := new(tabwriter.Writer)
		w.Init(os.Stdout, 0, 8, 0, '\t', 0)
		for range et.ticker.C {
			if !et.running {
				break
			}
			events, err := getCFEvents(et.session.CF, et.stackName)
			if err != nil && !strings.Contains(err.Error(), "does not exist") {
				fmt.Printf("Error getting stack events: %v\n", err)
			}
			for i := len(events) - 1; i >= 0; i-- {
				e := events[i]
				if _, exists := et.eventIds[*e.EventId]; !exists {
					reason := e.ResourceStatusReason
					if reason == nil {
						reason = aws.String("")
					}
					fmt.Fprintf(w, "%s\t%s\t%s\t%s\t%s\n", e.Timestamp.Format(time.RFC3339), color.BlueString(*e.ResourceType), color.GreenString(*e.LogicalResourceId), color.YellowString(*e.ResourceStatus), color.RedString(*reason))
					if *e.ResourceType == "AWS::CloudFormation::Stack" {
						et.stackStatus = *e.ResourceStatus
					}
					et.eventIds[*e.EventId] = true
				}
			}
			w.Flush()
		}
	}()
}

func (et *EventTailer) Stop() {
	if !et.running {
		return
	}
	et.running = false
	et.wg.Wait()
}

func getCFEvents(cf *cloudformation.CloudFormation, stack string) ([]cloudformation.StackEvent, error) {
	var newEvents []cloudformation.StackEvent
	err := cf.DescribeStackEventsPages(&cloudformation.DescribeStackEventsInput{
		StackName: &stack,
	}, func(p *cloudformation.DescribeStackEventsOutput, last bool) (shouldContinue bool) {
		for _, event := range p.StackEvents {
			newEvents = append(newEvents, *event)
		}
		return true
	})
	if err != nil {
		return nil, err
	}
	return newEvents, nil
}
