package cfutil

import (
	"errors"

	"github.com/aws/aws-sdk-go/service/cloudformation"
)

func (session *CFSession) GetStack(stackName string) (*cloudformation.Stack, error) {

	stack, err := session.CF.DescribeStacks(&cloudformation.DescribeStacksInput{
		StackName: &stackName,
	})
	if err != nil {
		return nil, err
	}
	if len(stack.Stacks) != 1 {
		return nil, errors.New("unable to find unique stack")
	}
	return stack.Stacks[0], nil
}

func (session *CFSession) ListResources(stackName string) ([]*cloudformation.StackResourceSummary, error) {
	summaries := make([]*cloudformation.StackResourceSummary, 0)
	err := session.CF.ListStackResourcesPages(&cloudformation.ListStackResourcesInput{
		StackName: &stackName,
	}, func(output *cloudformation.ListStackResourcesOutput, last bool) bool {
		for i := range output.StackResourceSummaries {
			summaries = append(summaries, output.StackResourceSummaries[i])
		}
		return true
	})
	return summaries, err
}
