package cfutil

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudformation"
)

type CFSession struct {
	CF     *cloudformation.CloudFormation
	region string
}

func NewCFSession(region string) *CFSession {
	sess := session.Must(session.NewSession(&aws.Config{
		Region: &region,
	}))
	return &CFSession{
		CF:     cloudformation.New(sess),
		region: region,
	}
}
