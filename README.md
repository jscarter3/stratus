# Stratus

Stratus is a utility that tries to make using cloudformation less painful

## Installation
Download from https://gitlab.com/jscarter3/stratus/releases[releases].

Or

```sh
go get -u gitlab.com/jscarter3/stratus
```

## Usage

```sh
Usage:
  stratus [command]

Available Commands:
  delete      Delete a cloudformation stack
  exports     list exports from a region
  get         Returns details of a cloudformation stack
  help        Help about any command
  kill        Stops all tasks within a given task family
  list        List all cloudformation stacks
  tail        Tail events from a cloudformation stack
  upsert      Create or update a cloudformation stack

Flags:
  -h, --help            help for stratus
  -r, --region string   aws region to use. defaults to AWS_REGION then AWS_DEFAULT_REGION environment variables
```

Stratus will use the AWS account specified via the default credential provider chain.
See http://docs.aws.amazon.com/sdk-for-go/api/[SDK documentation] for more details.

### Delete Command
Delete a cloudformation stack

```
Usage:
  stratus delete [flags]

Flags:
  -h, --help           help for delete
  -s, --stack string   stack to delete

Global Flags:
  -r, --region string   aws region to use. defaults to AWS_REGION then AWS_DEFAULT_REGION environment variables

```

### Exports Command
Lists all exports from a region in a Key/Value format. If a stack name is provided, all outputs
for that stack are also included

```
Usage:
  stratus exports [flags]

Flags:
  -h, --help           help for exports
  -k, --key string     optional key filter
  -s, --stack string   optional stack name filter

Global Flags:
  -r, --region string   aws region to use. defaults to AWS_REGION then AWS_DEFAULT_REGION environment variables
```

### Get Command
Returns details of a cloudformation stack

```
Usage:
  stratus get [flags]

Flags:
  -h, --help           help for get
  -s, --stack string   stack to get

Global Flags:
  -r, --region string   aws region to use. defaults to AWS_REGION then AWS_DEFAULT_REGION environment variables
```

### List Command
List all cloudformation stacks

```
Usage:
  stratus list [flags]

Flags:
  -h, --help   help for list

Global Flags:
  -r, --region string   aws region to use. defaults to AWS_REGION then AWS_DEFAULT_REGION environment variables
```

### Upsert Command
Create or update a cloudformation stack

```
Usage:
  stratus upsert [flags]

Flags:
  -f, --force                whether or not to force the upsert instead of asking
  -h, --help                 help for upsert
  -p, --parameter strslice   key:value parameter, can pass in multiple (default [])
  -s, --stack string         stack name to use
      --tag strslice         key:value tag, can pass in multiple (default [])
  -t, --template string      cloudformation template file to use

Global Flags:
  -r, --region string   aws region to use. defaults to AWS_REGION then AWS_DEFAULT_REGION environment variables
```

Example:
```
stratus upsert --region=us-east-2 --stack=stratus-test --template=stratus_template.yml \
    --parameter=Foo:bar \
    --parameter=SomeVariable:$somevar \
    --parameter=AreThreeParamExamplesEnough:true
```