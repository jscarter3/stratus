package cmd

import (
	"fmt"

	"encoding/json"

	"gitlab.com/jscarter3/stratus/cfutil"
	"github.com/spf13/cobra"
)

var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Returns details of a cloudformation stack",
}

var getOpts struct {
	StackName string
}

func init() {
	getCmd.Flags().StringVarP(&getOpts.StackName, "stack", "s", "", "stack to get")
	getCmd.Run = runGet
}

func runGet(cmd *cobra.Command, args []string) {
	cf := cfutil.NewCFSession(globalOpts.Region)
	stack, err := cf.GetStack(getOpts.StackName)
	checkErr(err)
	b, err := json.MarshalIndent(stack, "", "\t")
	fmt.Println(string(b))
	checkErr(err)
}
