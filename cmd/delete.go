package cmd

import (
	"fmt"

	"os"
	"text/tabwriter"

	"bufio"
	"strings"

	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/fatih/color"
	"gitlab.com/jscarter3/stratus/cfutil"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a cloudformation stack",
}

var deleteOpts struct {
	StackName string
}

func init() {
	deleteCmd.Flags().StringVarP(&deleteOpts.StackName, "stack", "s", "", "stack to delete")
	deleteCmd.PreRunE = validateParameters
	deleteCmd.Run = runDelete

}

func validateParameters(cmd *cobra.Command, args []string) error {
	if deleteOpts.StackName == "" {
		return errors.New("Stack name is required")
	}
	return nil
}

func runDelete(cmd *cobra.Command, args []string) {

	cf := cfutil.NewCFSession(globalOpts.Region)
	resources, err := cf.ListResources(deleteOpts.StackName)
	if err != nil {
		checkErr(errors.New("unable to find stack"))
	}
	fmt.Printf("The following resouces will be deleted:\n")
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 0, 8, 0, '\t', 0)
	for _, resource := range resources {
		fmt.Fprintf(w, "%s\t%s\n", color.BlueString(*resource.ResourceType), color.GreenString(*resource.LogicalResourceId))
	}
	w.Flush()

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Do you want to continue? [y/N] ")
	text, _ := reader.ReadString('\n')
	if strings.ToLower(text)[0] != 'y' {
		return
	}

	tail := cf.NewEventTail(deleteOpts.StackName)
	tail.Start()

	_, err = cf.CF.DeleteStack(&cloudformation.DeleteStackInput{
		StackName: &deleteOpts.StackName,
	})
	checkErr(err)
	err = cf.CF.WaitUntilStackDeleteComplete(&cloudformation.DescribeStacksInput{
		StackName: &deleteOpts.StackName,
	})
	tail.Stop()
	checkErr(err)
}
