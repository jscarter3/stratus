package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var resourcesCmd = &cobra.Command{
	Use:   "resources",
	Short: "list resources from a region",
	//	Long: `Lists all exports from a region in a Key/Value format. If a stack name is provided, all outputs
	// for that stack are also included`,
}

var resourcesOpts struct {
	StackName string
	Key       string
}

func init() {
	resourcesCmd.Flags().StringVarP(&resourcesOpts.StackName, "stack", "s", "", "optional stack name filter")
	resourcesCmd.Flags().StringVarP(&resourcesOpts.Key, "key", "k", "", "optional key filter")
	resourcesCmd.Run = runResources
}

func runResources(cmd *cobra.Command, args []string) {
	fmt.Printf("Not implemented yet\n")
	// cf := cfutil.NewCFSession(globalOpts.Region)
	//
	// resources, err := cf.ListResources()DescribeStacksPages(&cloudformation.DescribeStacksInput{}, func(o *cloudformation.DescribeStacksOutput, b bool) bool {
	// 	for _, stack := range o.Stacks {
	// 		fmt.Printf("%s - %s\n", *stack.StackName, *stack.StackStatus)
	// 	}
	// 	return true
	// })
	// checkErr(err)
}
