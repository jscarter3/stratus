package cmd

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/spf13/cobra"
)

var exportsCmd = &cobra.Command{
	Use:   "exports",
	Short: "list exports from a region",
	Long: `Lists all exports from a region in a Key/Value format. If a stack name is provided, all outputs 
for that stack are also included`,
}

var exportOpts struct {
	StackName string
	Key       string
}

func init() {
	exportsCmd.Flags().StringVarP(&exportOpts.StackName, "stack", "s", "", "optional stack name filter")
	exportsCmd.Flags().StringVarP(&exportOpts.Key, "key", "k", "", "optional key filter")
	exportsCmd.Run = runExports
}

type KeyValue struct {
	Key   string
	Value string
}

func runExports(cmd *cobra.Command, args []string) {
	sess := session.Must(session.NewSession(&aws.Config{
		Region: &globalOpts.Region,
	}))

	cf := cloudformation.New(sess)

	var allOutputs []KeyValue
	if exportOpts.StackName != "" {
		stack, err := cf.DescribeStacks(&cloudformation.DescribeStacksInput{
			StackName: aws.String(exportOpts.StackName),
		})
		checkErr(err)
		if len(stack.Stacks) != 1 {
			checkErr(errors.New("unable to find unique stack"))
		}
		for _, output := range stack.Stacks[0].Outputs {
			allOutputs = append(allOutputs, KeyValue{Key: *output.OutputKey, Value: *output.OutputValue})
		}
	} else {
		err := cf.ListExportsPages(&cloudformation.ListExportsInput{}, func(output *cloudformation.ListExportsOutput, b bool) bool {
			for _, export := range output.Exports {
				allOutputs = append(allOutputs, KeyValue{Key: *export.Name, Value: *export.Value})
			}
			return true
		})
		checkErr(err)
	}

	if exportOpts.Key != "" {
		for _, kv := range allOutputs {
			if kv.Key == exportOpts.Key {
				b, err := json.MarshalIndent(kv, "", "\t")
				fmt.Println(string(b))
				checkErr(err)
				return
			}
		}
		checkErr(errors.New("key not found"))
	} else {
		if len(allOutputs) > 0 {
			b, err := json.MarshalIndent(allOutputs, "", "\t")
			fmt.Println(string(b))
			checkErr(err)
		}
	}
}
