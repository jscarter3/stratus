package cmd

import (
	"fmt"
	"os"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var StratusCmd = &cobra.Command{
	Use:   "stratus",
	Short: "cloudformation utility",
	Long:  "Stratus is a tool that provides helpful abstractions for working with AWS CloudFormation",
}

var globalOpts struct {
	Region string
}

func Execute() {
	StratusCmd.AddCommand(listCmd)
	StratusCmd.AddCommand(exportsCmd)
	StratusCmd.AddCommand(getCmd)
	StratusCmd.AddCommand(deleteCmd)
	StratusCmd.AddCommand(upsertCmd)
	StratusCmd.AddCommand(resourcesCmd)

	StratusCmd.PersistentFlags().StringVarP(&globalOpts.Region, "region", "r", "", "aws region to use. defaults to AWS_REGION then AWS_DEFAULT_REGION environment variables")
	StratusCmd.PersistentPreRun = defaultRegion
	if _, err := StratusCmd.ExecuteC(); err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(-1)
	}
}

func checkErr(err error) {
	if err == nil {
		return
	} else {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}
}

func defaultRegion(cmd *cobra.Command, args []string) {
	if globalOpts.Region != "" {
		return
	} else if os.Getenv("AWS_REGION") != "" {
		globalOpts.Region = os.Getenv("AWS_REGION")
		return
	} else if os.Getenv("AWS_DEFAULT_REGION") != "" {
		globalOpts.Region = os.Getenv("AWS_DEFAULT_REGION")
		return
	}
	checkErr(errors.New("region not defined as parameter or as environment variable"))
}
