package cmd

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"os/signal"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/fatih/color"
	"gitlab.com/jscarter3/stratus/cfutil"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var upsertCmd = &cobra.Command{
	Use:   "upsert",
	Short: "Create or update a cloudformation stack",
}

type strslice []string

func (s *strslice) String() string {
	return fmt.Sprintf("%s", *s)
}

func (s *strslice) Set(value string) error {
	*s = append(*s, value)
	return nil
}

func (s *strslice) Type() string {
	return "strslice"
}

var upsertOpts struct {
	Force         bool
	StackName     string
	Template      string
	ParamSlice    strslice
	Parameters    map[string]string
	TagSlice      strslice
	Tags          map[string]string
	ParameterFile string
}

func init() {
	upsertCmd.Flags().StringVarP(&upsertOpts.StackName, "stack", "s", "", "stack name to use")
	upsertCmd.Flags().StringVarP(&upsertOpts.Template, "template", "t", "", "cloudformation template file to use")
	upsertCmd.Flags().VarP(&upsertOpts.ParamSlice, "parameter", "p", "key:value parameter, can pass in multiple")
	upsertCmd.Flags().VarP(&upsertOpts.ParamSlice, "tag", "", "key:value tag, can pass in multiple")
	upsertCmd.Flags().BoolVarP(&upsertOpts.Force, "force", "f", false, "whether or not to force the upsert instead of asking")
	upsertCmd.PreRun = parseParametersAndTags
	upsertCmd.Run = runUpsert
}

func parseParametersAndTags(cmd *cobra.Command, args []string) {
	upsertOpts.Parameters = make(map[string]string)
	for _, p := range upsertOpts.ParamSlice {
		kv := strings.Split(p, ":")
		if len(kv) < 2 {
			checkErr(errors.New("unable to parse parameter: " + p))
		}
		upsertOpts.Parameters[kv[0]] = strings.Join(kv[1:], ":")
	}
	upsertOpts.Tags = make(map[string]string)
	for _, p := range upsertOpts.TagSlice {
		kv := strings.Split(p, ":")
		if len(kv) < 2 {
			checkErr(errors.New("unable to parse parameter: " + p))
		}
		upsertOpts.Tags[kv[0]] = strings.Join(kv[1:], ":")
	}
}

func runUpsert(cmd *cobra.Command, args []string) {
	tbb, err := ioutil.ReadFile(upsertOpts.Template)
	checkErr(err)
	tb := string(tbb)

	var params []*cloudformation.Parameter

	for k, v := range upsertOpts.Parameters {
		params = append(params, &cloudformation.Parameter{
			ParameterKey:   aws.String(k),
			ParameterValue: aws.String(v),
		})
	}

	var tags []*cloudformation.Tag
	for k, v := range upsertOpts.Tags {
		tags = append(tags, &cloudformation.Tag{
			Key:   aws.String(k),
			Value: aws.String(v),
		})
	}

	cfSession := cfutil.NewCFSession(globalOpts.Region)

	exists := true
	changeSetType := "UPDATE"
	_, err = cfSession.GetStack(upsertOpts.StackName)
	if err != nil {
		exists = false
		changeSetType = "CREATE"
	}

	cso, err := cfSession.CF.CreateChangeSet(&cloudformation.CreateChangeSetInput{
		ChangeSetName: aws.String(fmt.Sprintf("%s-%d", upsertOpts.StackName, rand.Int())),
		ChangeSetType: &changeSetType,
		StackName:     &upsertOpts.StackName,
		TemplateBody:  &tb,
		Capabilities:  []*string{aws.String("CAPABILITY_NAMED_IAM")},
		Parameters:    params,
		Tags:          tags,
	})

	checkErr(errors.Wrap(err, "error creating change set"))

	var dso *cloudformation.DescribeChangeSetOutput
	for {
		dso, err = cfSession.CF.DescribeChangeSet(&cloudformation.DescribeChangeSetInput{
			StackName:     &upsertOpts.StackName,
			ChangeSetName: cso.Id,
		})
		checkErr(errors.Wrap(err, "error describing change set"))
		if strings.Contains(*dso.Status, "COMPLETE") {
			break
		} else if strings.Contains(*dso.Status, "FAILED") {
			cfSession.CF.DeleteChangeSet(&cloudformation.DeleteChangeSetInput{ //TODO should probably add error handling for this
				StackName:     &upsertOpts.StackName,
				ChangeSetName: cso.Id,
			})
			if strings.Contains(*dso.StatusReason, "submitted information didn't contain changes") {
				fmt.Printf("No updates to perform on stack %s\n", upsertOpts.StackName)
				return
			} else {
				checkErr(errors.New(fmt.Sprintf("failed to create change set - %s", *dso.StatusReason)))
			}
		}
		time.Sleep(1 * time.Second)
	}
	if len(dso.Changes) == 0 {
		fmt.Printf("No updates to perform on stack %s\n", upsertOpts.StackName)
		return
	}
	fmt.Printf("The following changes will be made:\n")
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 0, 8, 0, '\t', 0)
	for _, change := range dso.Changes {
		details := make([]string, 0)
		for _, detail := range change.ResourceChange.Details {
			details = append(details, *detail.Target.Name)
		}
		isReplacement := ""
		if change.ResourceChange != nil && change.ResourceChange.Replacement != nil && *change.ResourceChange.Replacement == "True" {
			isReplacement = "Requires replacement"
		}
		fmt.Fprintf(w, "%s\t%s\t%s\t%s\t%s\n", *change.Type, color.BlueString(*change.ResourceChange.ResourceType), color.GreenString(*change.ResourceChange.LogicalResourceId), color.YellowString(strings.Join(details, ",")), color.RedString(isReplacement))
	}
	w.Flush()

	if !upsertOpts.Force {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Do you want to continue? [y/N] ")
		text, _ := reader.ReadString('\n')
		if strings.ToLower(text)[0] != 'y' {
			if changeSetType == "UPDATE" {
				_, err = cfSession.CF.DeleteChangeSet(&cloudformation.DeleteChangeSetInput{
					StackName:     &upsertOpts.StackName,
					ChangeSetName: cso.Id,
				})
				checkErr(errors.Wrap(err, "error deleting change set"))
			} else if changeSetType == "CREATE" {
				_, err = cfSession.CF.DeleteStack(&cloudformation.DeleteStackInput{
					StackName: &upsertOpts.StackName,
				})
				checkErr(errors.Wrap(err, "error deleting stack"))
			}
			return
		}
	}

	tailer := cfSession.NewEventTail(upsertOpts.StackName)
	tailer.Start()

	_, err = cfSession.CF.ExecuteChangeSet(&cloudformation.ExecuteChangeSetInput{
		StackName:     &upsertOpts.StackName,
		ChangeSetName: cso.Id,
	})
	checkErr(errors.Wrap(err, "error executing change set"))

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt)

	go func() {
		<-sigc
		_, err = cfSession.CF.CancelUpdateStack(&cloudformation.CancelUpdateStackInput{
			StackName: &upsertOpts.StackName,
		})
	}()

	if exists {
		err = cfSession.CF.WaitUntilStackUpdateComplete(&cloudformation.DescribeStacksInput{
			StackName: &upsertOpts.StackName,
		})
	} else {
		err = cfSession.CF.WaitUntilStackCreateComplete(&cloudformation.DescribeStacksInput{
			StackName: &upsertOpts.StackName,
		})
	}
	tailer.Stop()
	lastStatus := tailer.StackStatus()
	if strings.Contains(lastStatus, "ROLLBACK") || strings.Contains(lastStatus, "FAILED") {
		checkErr(errors.New(fmt.Sprintf("Upsert failed - %s\n", lastStatus)))
	}
	checkErr(err)
}
