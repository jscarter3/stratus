package cmd

import (
	"fmt"
	"sync"

	"os"
	"text/tabwriter"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all cloudformation stacks",
	Run:   runList,
}

func runList(cmd *cobra.Command, args []string) {
	resolver := endpoints.DefaultResolver()
	partitions := resolver.(endpoints.EnumPartitions).Partitions()

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 0, 8, 0, '\t', 0)

	var wg sync.WaitGroup

	for _, p := range partitions {
		if "aws" != p.ID() {
			continue
		}
		for id := range p.Regions() {
			wg.Add(1)
			go func(region string) {
				defer wg.Done()
				sess := session.Must(session.NewSession(&aws.Config{
					Region: aws.String(region),
				}))

				cf := cloudformation.New(sess)
				err := cf.ListStacksPages(&cloudformation.ListStacksInput{}, func(p *cloudformation.ListStacksOutput, last bool) (shouldContinue bool) {
					for _, stack := range p.StackSummaries {
						if *stack.StackStatus != "DELETE_COMPLETE" {
							fmt.Fprintf(w, "Region: %s\tStack: %s\tStatus: %s\n", color.BlueString(region), color.GreenString(*stack.StackName), color.YellowString(*stack.StackStatus))
						}
					}
					return true
				})

				if err != nil {
					fmt.Fprintf(w, "Region: %s\tError: %s\n", color.BlueString(region), err)
				}
				w.Flush()
			}(id)
		}
	}
	wg.Wait()
}
